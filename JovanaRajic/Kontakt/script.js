let ime = document.getElementById("Ime");
let prezime = document.getElementById("Prezime ");
let kontakt = document.getElementById("Kontakt");
let dugme = document.getElementById("dugme");

let errorIme = document.getElementById("erorIme");
let errorPrezime = document.getElementById("erorPrezime");
let errorKontakt = document.getElementById("erorKontakt");
let errorDugme = document.getElementById("erordugme");
let info =JSON.parse(localStorage?.getItem('info')) || [];

if (info.length !== 0) ShowAndClear()

function Insert(){
    let noviKontakt = {
        ime: ime.value,
        prezime: prezime.value,
        kontakt: kontakt.value
    }
    if (ime.value === "" || prezime.value === "" || kontakt.value === "") {
        errorDugme.textContent = "Morate popuniti sva polja"
    }
    else if (dugme.textContent.toLowerCase() === "potvrdi" &&
        info.filter(e => e.kontakt === kontakt.value).length !== 0) {
        errorDugme.textContent = "Broj postoji u bazi"
    }
    else {
        if (dugme.textContent.toLowerCase() === "potvrdi") {
            errorDugme.textContent = ""
            info.push(noviKontakt)
            localStorage.setItem("info", JSON.stringify(info))
            ShowAndClear();
        }
        else {
            let i = dugme.value;
            info.splice(i, 1, noviKontakt);
            localStorage.setItem("info", JSON.stringify(info));
            ShowAndClear();
        }
    }
}

function Show(arr){
    let rez = document.getElementById("ispis");
    let a= "";
    for (let i = 0; i < arr.length; i++) {
        console.log();
        a+=`
        <tr>
        <td>${arr[i].ime}</td>
        <td>${arr[i].prezime}</td>
        <td>${arr[i].kontakt}</td>
        <td><button onclick = "Delete(info,${i})">Delete</button></td>
        <td><button onclick = "Edit(info,${i})">Edit</button></td>
        </tr>
        </tr>`
    }
    rez.innerHTML = a;
}

function Delete(niz, index) {
    niz.splice(index, 1)
    Show(niz);
}
function Edit(niz, index) {
    ime.value = niz[index].ime;
    prezime.value = niz[index].prezime;
    kontakt.value = niz[index].kontakt;
    dugme.textContent = "sacuvaj";
    dugme.value = index;
}
function ShowAndClear(){
    Show(info);

    ime.value="";
    prezime.value="";
    kontakt.value="";
    dugme.textContent='Potvrdi'
}