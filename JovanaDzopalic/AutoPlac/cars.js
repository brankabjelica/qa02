let registracija = document.getElementById("registracija");
let kilometraza = document.getElementById("km");
let brend = document.getElementById("brend");
let godiste = document.getElementById("godiste");
let dugme = document.getElementById("dugme");

let errorRed = document.getElementById("erorRed")
let errorKm = document.getElementById("erorKm")
let errorBrend = document.getElementById("erorBrend")
let errorGodiste = document.getElementById("errorGodiste")
let errorDugme = document.getElementById("erorDugme")


let automobili = JSON.parse(localStorage?.getItem('automobili')) || [];

function Insert() {

    let NoviAuto = {
        brend: brend.value,
        godiste: godiste.value,
        registracija: registracija.value,
        kilometraza: kilometraza.value
    }
    
    if (brend.value === "" || godiste.value === "" || registracija.value === "" || kilometraza.value === "") {

        errorDugme.textContent = "Morate popuniti sva polja"
    }
    else if (dugme.textContent.toLowerCase() === "potvrdi" &&
        automobili.filter(e => e.registracija === registracija.value).length !== 0) {
        errorDugme.textContent = "Registracija postoji u bazi"
    }
    else {
        if (dugme.textContent.toLowerCase() === "potvrdi") {

            errorDugme.textContent = ""
            automobili.push(NoviAuto)

            localStorage.setItem("automobili", JSON.stringify(automobili))
            ShowAndClear();
        }
        else {
            let i = dugme.value;
            automobili.splice(i, 1, NoviAuto)
            localStorage.setItem("automobili", JSON.stringify(automobili))
            ShowAndClear()
        }
    }
}

function Show(arr) {
    let a = "";
    let b = "";
    let c = "";
    console.log(arr)
    for (let i = 0; i < arr.length; i++) {
        if ( Number(arr[i].kilometraza) < 50000) {
            a += `
            <tr>
            <td>${arr[i].registracija}</td>
            <td>${arr[i].kilometraza}</td>
            <td>${arr[i].brend}</td>
            <td>${arr[i].godiste}</td>
                <td><button onclick = "Delete(automobili,${i})">Delete</button></td>
                <td><button onclick = "Edit(automobili,${i})">Edit</button></td>
            </tr>
            </tr>`
        }
        else if (Number(arr[i].kilometraza) > 50000 && Number(arr[i].kilometraza) < 150000) {
            b += `
                <tr>
                <td>${arr[i].registracija}</td>
                <td>${arr[i].kilometraza}</td>
                <td>${arr[i].brend}</td>
                <td>${arr[i].godiste}</td>
                <td><button onclick = "Delete(automobili,${i})">Delete</button></td>
                <td><button onclick = "Edit(automobili,${i})">Edit</button></td>
                </tr>
                </tr>`
        }
        else {
            c += `
                    <tr>
                    <td>${arr[i].registracija}</td>
                    <td>${arr[i].kilometraza}</td>
                    <td>${arr[i].brend}</td>
                    <td>${arr[i].godiste}</td>
                    <td><button onclick = "Delete(automobili,${i})">Delete</button></td>
                    <td><button onclick = "Edit(automobili,${i})">Edit</button></td>
                    </tr>
                    </tr>`
        }
    }
document.getElementById("tabela0").innerHTML = a
document.getElementById("tabela1").innerHTML = b
document.getElementById("tabela2").innerHTML = c
}


function Delete(niz,index) {

    niz.splice(index, 1)
    Show(niz)
}

function Edit(niz, index) {

    brend.value = niz[index].brend;
    godiste.value = niz[index].godiste;
    registracija.value = niz[index].registracija;
    kilometraza.value = niz[index].kilometraza;
    dugme.textContent = "Sacuvaj";
    dugme.value = i;


}

function Brend() {
    let brendname = document.getElementById("brend");
    document.getElementsByClassName("tabela").innerHTML = brendname;
}

function Godiste() {
    let godistename = document.getElementById("godiste");
    document.getElementsByClassName("tabela").innerHTML = godistename;
}

function ShowAndClear() {
    Show(automobili);

    registracija.value = "";
    brend.value = "";
    godiste.value = "";
    kilometraza.value = "";
    dugme.textContent = 'Potvrdi'
}



